# https://python-scripts.com/create-gif-in-pil

from PIL import Image
 
# Список для хранения кадров.
frames = []

Width = 900
Height = 600
 
for frame_number in range(6, 101):
    # Открываем изображение каждого кадра.
    frame = Image.open(f'img/{frame_number}.png')
    frame = frame.resize((Width,Height))
    # Добавляем кадр в список с кадрами.
    frames.append(frame)
 
# Берем первый кадр и в него добавляем оставшееся кадры.
frames[0].save(
    'new.gif',
    save_all=True,
    append_images=frames[1:],  # Срез который игнорирует первый кадр.
    optimize=True,
    duration=100,
    loop=0
)