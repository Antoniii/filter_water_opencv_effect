# conda activate iWM

# https://beerensahu.wordpress.com/2021/02/03/wave-transformation-warping-augmentation-in-python/2/

from skimage.io import imread
from skimage.transform import warp
import numpy as np
import matplotlib.pyplot as plt
 

def wave(xy):
    xy[:, 1] += A*np.sin(2*np.pi*xy[:, 0]/B)
    return xy

im = imread('esher.jpg') # read image in skimage.io

i = 1

for A in range(1, 21, 2):
	for B in range(2, 64, 4):

		im = warp(im, wave) # apply transformation

		plt.figure(frameon=False)
		plt.imshow (im)
		plt.axis('off')
		     
		plt.savefig(f"img/{i}.png", facecolor="orange")
		i += 1

for A in range(20, 1, -2):
	for B in range(62, 2, -4):

		im = warp(im, wave) # apply transformation

		plt.figure(frameon=False)
		plt.imshow (im)
		plt.axis('off')
		     
		plt.savefig(f"img/{i}.png", facecolor="orange")
		i += 1