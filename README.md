![](https://gitlab.com/Antoniii/filter_water_opencv_effect/-/raw/main/old.gif)


## Sources

* [Open-your-eyes-open-your-mind-OpenCV](https://github.com/unton3ton/Open-your-eyes-open-your-mind-OpenCV)
* [Image Augmentation | Wave transformation or warping in Python](https://beerensahu.wordpress.com/2021/02/03/wave-transformation-warping-augmentation-in-python/2/)
* [Python: PS filter-water potter effect](https://www.programmersought.com/article/57346064555/)
* [plt.figure убрать границу](https://russianblogs.com/article/68321554046/)
* [Создание GIF анимации в Python](https://python-scripts.com/create-gif-in-pil)
